# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Node.js server for bigmir onlines
Using protocol ws or (better) wss

### How do I get set up? ###

Usage directly from repository:
 docker run -it --rm -p 8099:80 -e "BMNODE_PORT=8099" ivanezko/bmnode

Or, if you use nginx:
docker run -it --rm -p 8099:127.0.0.1:8099 -e "BMNODE_PORT=8099" ivanezko/bmnode

### Original Dockerfile ###
```
FROM node:0.10.43

ENV BMNODE_PORT 8089

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app

CMD [ "node", "server_module.js" ]
```
### Testing ###

http://lena.ivanezko.com:8099/socket.io/socket.io.js
replace "lena.ivanezko.com" with your domain

### Nginx ###
```
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

upstream bmnodejs {
    server localhost:8099;
}

server {
        listen                  80;
        listen                  443 ssl;
        server_name             bmnode.devzone.co.ua;

        root                    /ANY_FOLDER;
        access_log              /var/log/nginx/bigmir/bmnode;
        #expires                        epoch;
        #client_body_timeout 1m;
        #client_max_body_size 100M;

        ssl on;
        ssl_certificate /PATH/server.crt;
        ssl_certificate_key /PATH/server.key;
        ssl_session_cache shared:SSL:10m;
        ssl_session_timeout 60m;


        location / {
            proxy_pass http://bmnodejs;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
        }
}
```